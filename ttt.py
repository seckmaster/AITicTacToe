import random
import time
import pickle
import os.path

data = None


class Board:

    @staticmethod
    def isgameover(board):
        """ analyze the board
        return 'X' or 'O' if player has won,
        return -1 if tie,
        return 0 if game is not over yet
        """
        x_fields, o_fields, width = board

        # if player hasn't made 3 moves, game can't be over yet
        if len(x_fields) < 3:
            return 0

        # sets of winning combinations
        # rows  = [{0, 1, 2}, {3, 4, 5}, {6, 7, 8}]
        # cols  = [{0, 3, 6}, {1, 4, 7}, {2, 5, 8}]
        # diags = [{0, 4, 8}, {2, 4, 6}]
        rows = [{width * j + i for i in range(width)} for j in range(width)]
        cols = [{width * i + j for i in range(width)} for j in range(width)]
        diags = [{width * i + i for i in range(width)},
                 {width * i + width - i - 1 for i in range(width)}]

        lines = rows + cols + diags

        # did either X or O win
        x_won = any(line.issubset(x_fields) for line in lines)
        o_won = any(line.issubset(o_fields) for line in lines)

        if x_won:
            return 'X'
        if o_won:
            return 'O'

        # check for tie
        if len(x_fields) + len(o_fields) == 9:
            return -1

        return False

    @staticmethod
    def generatemoves(board):
        """ return a set of all empty squares
        """
        x_squares, o_squares, width = board
        return set({i for i in range(width ** 2)} - (x_squares | o_squares))

    @staticmethod
    def getwinningsquares(board):
        """ find winning squares for both players
        and return tuple of both sets
        """
        x_squares, o_squares, width = board
        # todo for n in a row
        combinations = [[0, 1, 2], [1, 2, 0], [3, 4, 5], [4, 5, 3], [6, 7, 8], [7, 8, 6],  # horizontal combinations
                        [0, 3, 6], [3, 6, 0], [1, 4, 7], [4, 7, 0], [2, 5, 8], [5, 8, 2],  # vertical combinations
                        [0, 4, 8], [4, 8, 0], [2, 4, 6], [4, 6, 2]]                        # diagonal combinations

        x = set()
        o = set()
        for c in combinations:
            comb = {c[0], c[1]}
            square = c[2]
            if comb.issubset(x_squares):
                if square not in x_squares and square not in o_squares:
                    x |= {square}

            if comb.issubset(o_squares):
                if square not in x_squares and square not in o_squares:
                    o |= {square}
        return x, o

    @staticmethod
    def printboard(board):
        """ print board to std out
        """
        x_fields, o_fields, width = board
        print("   ", end="")
        for i in range(width):
            print(i, " ", end=" ")
        print()
        for i in range(width):
            print(i, " ", end="")
            for j in range(width):
                x = i * width + j
                if x in x_fields:
                    print("X   ", end="")
                elif x in o_fields:
                    print("O   ", end="")
                else:
                    print("    ", end="")
            print()
        print(end="")

    @staticmethod
    def hash(board):
        """ return unique string representation of the board
        """
        x_squares, o_squares, width = board
        hash = ""

        for i in x_squares:
            hash += str(i)
        hash += 'X'
        for i in o_squares:
            hash += str(i)
        hash += 'O'

        return hash


class TicTacToe:
    def __init__(self, n, p, player1, player2):
        # try:
        starttime = time.time()
        db = {'X': 0, 'O': 0, -1: 0}
        for i in range(n):
            if p:
                print("New game ... \n")
            db[self.newgame(player1, player2, width=3, display=p)] += 1

        # statistics
        print("Number of games:", n)
        print("Player1:", db['X'], db['X'] * 100 / n, "%")
        print("Player2:", db['O'], db['O'] * 100 / n, "%")
        print("Draws:", db[-1], db[-1] / n)

        print("Time elapsed [ms]:", (time.time() - starttime) * 1000)

        #except Exception as e:  # keyboard interrupt
        #    print("\nGoodbye!", e)


    def newgame(self, X, O, width=3, display=True):
        """ start a new TicTacToe game
        X - function for player 'X'
        O - function for player 'O'
        width - size of a board (default 3)
        display - print UI (default True)
        return value is either 'X', 'O' or -1
        """
        for board in self.play(X, O, width, display):
            if display:
                Board.printboard(board)

            winner = Board.isgameover(board)
            if winner in {'X', 'O'}:
                if display:
                    print("Game over,", winner, "is the winner!")
                return winner
            if winner == -1:
                if display:
                    print("Game over, game is tied")
                return winner

    def play(self, X, O, width, display):
        """ next step (move) in a TicTacToe
        based on whose turn is, call fn X or O to retrieve next move
        This function is in fact a generator and is yielding board states after every move
        """
        # new instance of a board
        # board is represented as tuple of two sets (one set for each player, and width)
        board = (set(), set(), width)
        # starting player is 'X'
        turn = 'X'
        while not Board.isgameover(board):
            if display:
                print("Next to play: ", turn)
            if turn == 'X':
                board[0].add(X(board, turn))
            else:
                board[1].add(O(board, turn))
            # yield current state of a board
            yield board
            # and swap players
            turn = {'O': 'X', 'X': 'O'}[turn]


class Players:
    def human(board, char):
        """ human player
        """
        x_squares, o_squares, width = board

        # square must not be occupied and has to be in range [0, width ** 2)
        while True:
            try:
                print("Enter location (e.g. 11 = 1, 1): ", end="")
                square = input()
                square = int(square[0]) + width * int(square[1])
                if 0 <= square < width ** 2 and \
                        not (square in x_squares or square in o_squares):
                    return square
                print()
            except:
                print("\nGoodbye!")
                exit(1)

    def ai_rnd(board, char):
        """ easy AI player
        play on random empty square
        unless there is a winning move (or loosing) and block it
        """
        x_squares, o_squares, width = board

        x, o = Board.getwinningsquares(board)
        if len(x):
            if len(o):
                return x.pop() if char == 'X' else o.pop()
            return x.pop()
        if len(o):
            return o.pop()

        return random.sample({i for i in range(0, 9)} - set.union(x_squares, o_squares), 1)[0]

    def ai_pref(board, char):
        """ AI player based on preference table
        next move is chosen from preference table, which consists of 3 arrays,
        each containg moves that are equally good
        """
        x_squares, o_squares, width = board

        x, o = Board.getwinningsquares(board)
        if len(x):
            if len(o):
                return x.pop() if char == 'X' else o.pop()
            return x.pop()
        if len(o):
            return o.pop()

        preftable = [[4], [0, 2, 6, 8], [1, 3, 5, 7]]
        for i in preftable:
            random.shuffle(i)
            for j in i:
                if j not in set.union(x_squares, o_squares):
                    return j

    def ai_minimax(board, char):
        """ A perfect player (it cannot be beaten) using minimax algorithm
        """
        return Players.minimax(9, board, char, -10, 10)[1]

    def ai_learn(board, char):
        """ AI player (agent) who learns to play the game by itself from scratch
        """
        global data

        # computer learns to play
        if not data:
            if os.path.isfile("data.dat"):
                data = pickle.load(open("data.dat", "rb"))
            else:
                # 30000 seems like a number where AI starts a decent play
                Players.gamelearning(board, 30000, char)
                # save data
                pickle.dump(data, open("data.dat", "wb"))

        #print(data)
        #print()

        # get highest evaluated move
        square = Players.minmaxQvalue(max, Board.hash(board))[0]

        # if position is unknown to the agent
        # let him learn it by playing it for couple of times
        while square == -1:
            Players.gamelearning(board, 100, char)
            square = Players.minmaxQvalue(max, Board.hash(board))[0]

        return square

    # --- Q-learning ---
    def gamelearning(initialState, maxGames, player):
        """ Implementation of Q-learning algorithm
        https://en.wikipedia.org/wiki/Q-learning
        http://web.cs.swarthmore.edu/~meeden/cs63/f11/lab6.php
        """
        global data

        opponent = {'X': 'O', 'O': 'X'}[player]
        state = initialState
        games = 0

        # control likelihood of choosing the best action
        # lower values of k = more random decisions
        k = 0.05
        # rate by which k is increased
        k_update = 0.05
        # interval for updating k
        update = 3000

        if not data:
            data = dict()

        while games < maxGames:
            x_squares, o_squares, width = state

            # if this is an unknown state
            # add it to the dict
            key = Board.hash(state)
            if key not in data:
                Players.addkey(key, Board.generatemoves(state))

            #update k
            if games % update == 0 and k <= 1.0 - k_update:
                k += k_update
            if k > 1.0:
                k = 1.0

            t_action = Players.chooseaction(key, player, k)
            if t_action[0] is None:
                print(t_action, k)
                print("error")
            action = t_action[0]

            # create new board (state of a game)
            newState = (x_squares | {action} if player == 'X' else x_squares,
                        o_squares | {action} if player == 'O' else o_squares,
                        width)
            nextKey = Board.hash(newState)

            # if game is over, reward the player
            # reward is 1 if player won, 0 if he tied and -1 if he lost
            result = Board.isgameover(newState)
            reward = {'X': 1, 'O': -1, -1: 0, 0: 0}[result]

            # if new state is unknown
            # add it to the dict
            if nextKey not in data:
                Players.addkey(nextKey, Board.generatemoves(newState))

            # update Q values
            Players.updateQvalues(key, t_action, nextKey, reward, newState, player)

            # if game is over, start new game
            if result in {'X', 'O', -1}:
                state = initialState
                games += 1
            # else swap players
            else:
                state = newState
                player, opponent = opponent, player

    def chooseaction(key, player, k):
        """ Choose next action (move) for learning player to try out
        the strategy is to try and learn as many new positions as possible in the early stages of learning (exploration),
        but in the later stages, prefer moves which are already considered to be strong (explotation).
        """
        global data

        # naive approach
        # half the time explore and half the time exploit
        # x = random.uniform(0, 1)
        #if x >= 0.5:
        #    return data[key][random.randint(0, len(data[key]) - 1)]
        #return Players.minmaxQvalue(max, key)

        # advanced approach using roulette wheel selection
        actionTuples = sorted(data[key], key=lambda x: x[1])
        actions = [i[0] for i in actionTuples]
        values = [i[1] for i in actionTuples]

        if player == 'X':
            lowest = min(values)
        else:
            # reverse signs of each value in value list
            values = [-i for i in values]
            lowest = min(values)

        # normalization
        if lowest <= 0:
            constant = abs(lowest) + 0.1
            # add constant to each value to make all values positive
            values = [i + constant for i in values]

        # roulette wheel selection
        # calculate probabilities
        fitnessSum = sum(values)

        probSum = 0.0
        probabilities = []

        for i in values:
            f = probSum + (i / fitnessSum)
            probabilities.append(f)
            probSum = f
        probabilities[len(probabilities) - 1] = 1.0

        # selection
        rnd = random.uniform(0, 1) * k
        for i in range(len(probabilities)):
            if rnd < probabilities[i]:
                return actionTuples[i]
        return None, rnd, k

    def updateQvalues(key, action, nextKey, reward, nextState, player):
        """ update Q value of action
        """
        global data

        # The learning rate determines to what extent the newly acquired information will override the old information.
        # A factor of 0 will make the agent not learn anything,
        # while a factor of 1 would make the agent consider only the most recent information.
        learningRate = 0.5
        # The discount factor determines the importance of future rewards.
        # A factor of 0 will make the agent "myopic" (or short-sighted) by only considering current rewards,
        # while a factor approaching 1 will make it strive for a long-term high reward.
        discount = 0.5

        result = Board.isgameover(nextState)
        # if game isn't over
        # approximate rewards
        if result not in {'X', 'O', -1}:
            if player == 'X':
                reward += discount * Players.minmaxQvalue(min, nextKey)[1]
            else:
                reward += discount * Players.minmaxQvalue(max, nextKey)[1]

        change = learningRate * (reward - action[1])

        index = data[key].index(action)
        data[key][index] = (action[0], action[1] + change)

    def minmaxQvalue(fn, key):
        """ return min or max value in list of values
        """
        global data
        if key not in data:
            return -1, -1
        return fn([i for i in data[key]], key=lambda x: x[1])

    def addkey(key, moves):
        """ add new key to data dictionary
        give every move a random value
        """
        global data
        interval = 0.15
        data[key] = [(i, random.uniform(-interval, interval)) for i in moves]
    # ---------------

    # --- minimax ---
    def minimax(depth, board, maximizingPlayer, alpha, beta):
        """ NEGAMAX version of MINIMAX algorithm optimized with alpha-beta pruning (principal variation search)
        return tuple of board evaluation and the best square
        https://en.wikipedia.org/wiki/Negamax#Negamax_with_alpha_beta_pruning
        https://en.wikipedia.org/wiki/Principal_variation_search
        """
        x_squares, o_squares, width = board
        opponent = {'X': 'O', 'O': 'X'}[maximizingPlayer]

        # evaluate position
        #   if maximizing player won - return  poz. value
        #   if opponent won          - return  neg. value
        #   if tie                   - return  0
        state = Board.isgameover(board)
        if state in {'X', 'O', -1}:
            return {maximizingPlayer: 10 - (9 - depth), opponent: -10 + (9 - depth), -1: 0}[state], -1

        bestSquare = -1

        # shuffle moves so that AI isn't 'boring'
        moves = list(Board.generatemoves(board))
        random.shuffle(moves)

        first = True

        # iterate all possible moves (empty squares)
        for m in moves:
            # create new board (state of a game) for each move
            newState = (x_squares | {m} if maximizingPlayer == 'X' else x_squares,
                        o_squares | {m} if maximizingPlayer == 'O' else o_squares,
                        width)

            if not first:
                value = -Players.minimax(depth - 1, newState, opponent, -alpha - 1, -alpha)[0]
                if alpha < value < beta:
                    value = -Players.minimax(depth - 1, newState, opponent, -beta, -value)[0]
            else:
                value = -Players.minimax(depth - 1, newState, opponent, -beta, -alpha)[0]
            first = False

            # alpha-beta prunning
            if value >= beta:
                return value, m

            if value > alpha:
                alpha = value
                bestSquare = m

        return alpha, bestSquare


TicTacToe(30, False, Players.ai_learn, Players.ai_minimax)
